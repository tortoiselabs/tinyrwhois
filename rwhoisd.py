#!/usr/bin/python
#
# Copyright 2014 TortoiseLabs LLC.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied. See the License for the specific language governing
# permissions and limitations under the License.

import sys
import os
import ipaddress

# Configuration for POC records.
STREET_ADDRESS = '2323 Bryan St. Suite 1120'
CITY = 'Dallas'
STATE = 'TX'
POSTAL_CODE = '75201'
COUNTRY_CODE = 'US'
POC_NAME = 'Network Administrator'
ABUSE_NAME = 'Abuse Team'
POC_EMAIL = 'support@centarra.com'
ABUSE_EMAIL = 'abuse@centarra.com'
POC_PHONE = 'EMAIL ONLY'
ABUSE_PHONE = 'EMAIL ONLY'

# Prefix database path.
PREFIXDB_PATH = '/home/tinyrwhois/prefixes.conf'

SERIAL = 1

class Prefix(object):
    def __init__(self, ipblock, customer):
        global SERIAL

        self.ipblock = ipaddress.ip_network(ipblock)
        self.customer = customer
        self.id = SERIAL
        SERIAL = SERIAL + 1

    def __repr__(self):
        return "[Prefix: {0}]".format(self.ipblock)

PREFIXLIST = []
HOSTNAME = os.uname()[1]

def load_prefixes(filepath=PREFIXDB_PATH):
    file = open(filepath, 'r')
    for line in file.readlines():
        if line.startswith('#')
            continue
        data = line.strip('\r\n').split(None, 1)
        if len(data) < 2:
            continue
        PREFIXLIST.append(Prefix(data[0], data[1]))

def search(query):
    addr = ipaddress.ip_address(query)
    results = filter(lambda x: addr in x.ipblock, PREFIXLIST)

    if len(results):
        return results[0]
    return None

def error(code, errmsg):
    print '%error {0} {1}'.format(code, errmsg)
    exit(-1)

def display_result(prefix):
    print 'autharea={0}'.format(prefix.ipblock)
    print 'xautharea={0}'.format(prefix.ipblock)
    print 'network:Class-Name:network'
    print 'network:Auth-Area:{0}'.format(prefix.ipblock)
    print 'network:ID:NET-{0}.{1}'.format(prefix.id, prefix.ipblock)
    print 'network:Network-Name:IP Pool'
    print 'network:IP-Network:{0}'.format(prefix.ipblock)
    print 'network:IP-Network-Block:{0} - {1}'.format(prefix.ipblock.network_address, prefix.ipblock.broadcast_address)
    print 'network:Org-Name:{0}'.format(prefix.customer)
    print 'network:Street-Address:{0}'.format(STREET_ADDRESS)
    print 'network:City:{0}'.format(CITY)
    print 'network:State:{0}'.format(STATE)
    print 'network:Postal-Code:{0}'.format(POSTAL_CODE)
    print 'network:Country-Code:{0}'.format(COUNTRY_CODE)

    print 'contact:POC-Name:{0}'.format(POC_NAME)
    print 'contact:POC-Email:{0}'.format(POC_EMAIL)
    print 'contact:POC-Phone:{0}'.format(POC_PHONE)
    print 'contact:Tech-Name:{0}'.format(POC_NAME)
    print 'contact:Tech-Email:{0}'.format(POC_EMAIL)
    print 'contact:Tech-Phone:{0}'.format(POC_PHONE)
    print 'contact:Abuse-Name:{0}'.format(ABUSE_NAME)
    print 'contact:Abuse-Email:{0}'.format(ABUSE_EMAIL)
    print 'contact:Abuse-Phone:{0}'.format(ABUSE_PHONE)

if __name__ == '__main__':
    load_prefixes()
    print '%rwhois V-1.0,V-1.5:00090h:00 {0} (tinyrwhois)'.format(HOSTNAME)
    query = sys.stdin.readline().strip('\r\n')
    prefix = search(query)
    if prefix is None:
        error(1, 'Could not find authoritative area for {0}'.format(query))
    display_result(prefix)
    print '%ok'
