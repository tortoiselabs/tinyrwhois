# tinyrwhois

tinyrwhois is free but copyrighted software.  See COPYING for details.

tinyrwhois is a small RWHOIS 1.0/1.5 server.  It is not, in any way, compliant with the
real RWHOIS protocol, but is "good enough" to fake it with realworld RWHOIS clients.

It supports mapping IPv4 and IPv6 requests to records relating to those requests, using an
in-memory prefix database.  If you want something more advanced, we recommend looking at
Cloudware.

## prefix database

The prefix database is of the format:

<prefix>	<description>

Any amount of whitespace between the prefix and the description is allowed.  There must be
at least one byte of whitespace between the prefix and the description.  Only one pair of
prefix and description are allowed per line.

## setup

Configure your inetd to run rwhoisd.py on port 4321.  Edit rwhoisd.py to have the appropriate
POC records for your organization.

## support

There is no support.  You can open a bug if you find a problem though.  We might look at it,
or something.

